import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author Dan64
 */
public class Nim {

    int partidas;
    int partidasJugador;
    Random random;
    Scanner in;

    boolean partidaAcabada(int[] tablero) {

        boolean finPartida = false;
        int suma = 0;

        for (int i = 0; i < tablero.length; i++) {
            suma += tablero[i];
        }

        if (suma == 0) {
            finPartida = true;
        }

        return finPartida;
    }

    public boolean jugadaEsCorrecta(int[] tablero, int fila, int fichas) {

        boolean correcto;

        if (fila >= 0 && fila < tablero.length) {
            correcto = fichas > 0 && fichas <= tablero[fila];
        } else {
            correcto = false;
        }

        return correcto;
    }

    public void dibujarTablero(int[] tablero) {
        
        for (int i = 0; i < tablero.length; i++) {
            
            System.out.format("%d| ", i + 1);
            
            for (int j = 0; j < tablero[i]; j++) {
                System.out.print("o");
            }
            System.out.println();
        }
    }

    public int[] generarTablero(int filas) {

        int[] tablero = new int[filas];
        
        for (int i = 0; i < tablero.length; i++) {
            tablero[i] = random.nextInt(6) + 1;
        }
        
        return tablero;
    }

    public void jugarPartida() {

        int contadorPartidas;

        int filas;
        int[] tablero;
        int fila;
        int fichas;
        boolean esCorrecto;
        String entradaTeclado;
        Scanner tokenizer;

        contadorPartidas = partidasJugador / 3;
        filas = 2 + contadorPartidas;

        tablero = generarTablero(filas);

        System.out.println("*******");
        System.out.println("* NIM *");
        System.out.println("*******\n");

        do {
            dibujarTablero(tablero);
            
            //Player's turn
            do {
                
                System.out.print("\nJugador: Su jugada: ");

                entradaTeclado = in.nextLine();
                tokenizer = new Scanner(entradaTeclado);

                fila = tokenizer.nextInt();
                fichas = tokenizer.nextInt();

                esCorrecto = jugadaEsCorrecta(tablero, fila - 1, fichas);

                if (esCorrecto) {
                    tablero[fila - 1] -= fichas;
                } else {
                    System.out.print("\n** Jugada no valida **");
                }

                System.out.println();

            } while (!esCorrecto);

            //Comprobacion final de partida
            if (partidaAcabada(tablero)) {
                partidasJugador++;
                System.out.println("#### HAS GANADO ####");
            }

            //PC turn
            if (!partidaAcabada(tablero)) {
                do {
                    dibujarTablero(tablero);

                    fila = random.nextInt(tablero.length);

                    /*
                        Este While comprueba que no haya valor 0 en la posición 
                        indicada, dado que si no, la asignación de valor random
                        con un valor 0 hará que el programa se detenga.
                     */
                    while (tablero[fila] == 0) {
                        fila = random.nextInt(tablero.length);
                    }

                    fichas = random.nextInt(tablero[fila]) + 1;

                    esCorrecto = jugadaEsCorrecta(tablero, fila, fichas);

                    if (esCorrecto) {
                        tablero[fila] -= fichas;
                    } else {
                        System.out.println("** Jugada de PC no valida **");
                    }

                    System.out.format("\nCPU: Fila: %d, retiro %d fichas.\n\n", 
                            fila + 1, fichas);

                } while (!esCorrecto);

                if (partidaAcabada(tablero)) {
                    System.out.println("#### CPU WINS: FATALITY!!! ####");
                }
            }
        } while (!partidaAcabada(tablero));

        partidas++;

        System.out.format("\nPartidas Jugadas: %d. Ganadas por el jugador: %d.\n",
                partidas, partidasJugador);
    }

    public void run() {

        in = new Scanner(System.in);
        random = new Random();
        partidas = 0;
        partidasJugador = 0;
        String entradaTeclado;

        jugarPartida();

        do {
            System.out.print("\n¿Otra partida? [S/n]: ");
            entradaTeclado = in.nextLine();

            if (entradaTeclado.equals("s")) {
                
                System.out.println();
                jugarPartida();
                
            }

        } while (entradaTeclado.equals("s"));
        
        System.out.println();
        System.out.println("\"Por si no nos volvemos a ver: ");
        System.out.println(" buenos días, buenas tardes y buenas noches\"");
    }

    public static void main(String[] args) {
        new Nim().run();
    }
}